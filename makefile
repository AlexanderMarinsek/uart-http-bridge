CC = gcc
CFLAGS = -g -Wall -I.

# -- list of dependencies -> header files
DEPS =  requests/requests.h

# -- list of objet files
OBJ = 	main.o 								\
		requests/requests.o


# -- list of phony targets
.PHONY: clean

# -- default command
all: main

# -- make main module
main: $(OBJ)
	$(CC) $(CFLAGS) $^ -o $@

# -- object files assembly rule
%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c $< -o $@

# -- delete .o files in main directory and sub-directories
clean:
	rm *.o */*.o
