//#include "hash.h"
#include "requests/requests.h"

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include <string.h>         // memset
#include <unistd.h>			//Used for UART
#include <fcntl.h>			//Used for UART
#include <termios.h>		//Used for UART


void get_formated_timestamp (char *timestamp);

int main () {

	/* SETUP UART --------------------------------------------------------------
	 * At bootup, pins 8 and 10 are already set to UART0_TXD,
     * UART0_RXD (ie the alt0 function) respectively
     */
	int uart0_filestream = -1;
    char rx_buffer[1024];
    char serial_incoming[256];
    int rx_length = 0;
    int rx_buf_idx = 0;
    int req_buf_idx = 0;
    int copy_buffer = 0;

	/*OPEN THE UART
	 * The flags (defined in fcntl.h):
	 *	Access modes (use 1 of these):
	 *		O_RDONLY - Open for reading only.
	 *		O_RDWR - Open for reading and writing.
	 *		O_WRONLY - Open for writing only.
	 *
	 *	O_NDELAY / O_NONBLOCK (same function) -
     *      Enables nonblocking mode. When set read requests on the file can
     *      return immediately with a failure status if there is no input
     *      immediately available (instead of blocking). Likewise, write
     *      requests can also return immediately with a failure status if the
     *      output can't be written immediately.
	 *
	 *	O_NOCTTY -
     *      When set and path identifies a terminal device, open() shall not
     *      cause the terminal device to become the controlling terminal for
     *      the process.
     */
    /* Open in non blocking read/write mode */
    //uart0_filestream = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
    /*uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (uart0_filestream == -1)
	{
		printf("Error - Unable to open UART\n");
	}*/

	/* CONFIGURE THE UART
	 * The flags (defined in /usr/include/termios.h - see
     * http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
	 *	Baud rate:- B1200, ..., B9600, ..., B115200, ..., B4000000
	 *	CSIZE:- CS5, CS6, CS7, CS8
	 *	CLOCAL - Ignore modem status lines
	 *	CREAD - Enable receiver
	 *	IGNPAR = Ignore characters with parity errors
	 *	ICRNL -
     *      Map CR to NL on input (Use for ASCII comms where you want to auto
     *      correct end of line characters - don't use for bianry comms!)
	 *	PARENB - Parity enable
	 *	PARODD - Odd parity (else even)
     */
	struct termios options;
	tcgetattr(uart0_filestream, &options);
    /* Set baudrate */
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
    /* Disables the Parity Enable bit(PARENB),So No Parity   */
    options.c_cflag &= ~PARENB;
    /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    options.c_cflag &= ~CSTOPB;
    /* Clears the mask for setting the data size */
    options.c_cflag &= ~CSIZE;
    /* Set the data bits = 8 */
    options.c_cflag |=  CS8;
	/* No Hardware flow Control */
    options.c_cflag &= ~CRTSCTS;
    /* Enable receiver,Ignore Modem Control lines */
	options.c_cflag |= CREAD | CLOCAL;
    /* Disable XON/XOFF flow control both i/p and o/p */
	options.c_iflag &= ~(IXON | IXOFF | IXANY);
    /* Non Cannonical mode */
	options.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    /* No Output Processing */
	options.c_oflag &= ~OPOST;

	tcflush(uart0_filestream, TCIFLUSH);
	int tcs = tcsetattr(uart0_filestream, TCSANOW, &options);
    //printf("tcsetattr(): %d\n", tcs);


    /* Init request structures -----------------------------------------------*/
    request_link_t request_link_anemosi;
    request_link_anemosi.portno = 80;
    sprintf(request_link_anemosi.host_addr,
    //    "10.0.0.5");
        "test.anemo.si");
    sprintf(request_link_anemosi.request_path,
    //    "/anemo-web/anemo_general_api.php");
        "/anemo_general_api.php");

    request_link_t request_link_node;
    request_link_node.portno = 5761;
    sprintf(request_link_node.host_addr,
        "10.0.0.5");
    sprintf(request_link_node.request_path,
        "/measurement-data");

    setup_socket(&request_link_anemosi);
    setup_socket(&request_link_node);


    //char serial_incoming[256];
    char data_buffer[256];
    char timestamp [64];


    // "hash=3906273145922897&timestamp=%s&w_speed=0.00&w_dir=000&w_gust_max=0.00&w_gust_peak=0"
    sprintf(serial_incoming,
        "{\"hash\":\"3906273145922897\",\"w_speed\":\"0001\",\"w_dir\":\"0002\",\"w_gust_max\":\"0003\",\"w_gust_peak\":\"1\"}");

    size_t serial_incoming_len;
    size_t req_buf_len;

	serial_incoming_len = strlen(serial_incoming);
	json_to_request_params(serial_incoming, serial_incoming_len);
    get_formated_timestamp(timestamp);
    sprintf(request_link_node.data_buffer,
        "timestamp=%s&%s", timestamp, serial_incoming);
    write_socket_blocking(&request_link_node);
    sprintf(request_link_anemosi.data_buffer,
        "timestamp=%s&%s", timestamp, serial_incoming);
    write_socket_blocking(&request_link_anemosi);


    while (1) {
        /*  CHECK FOR ANY RX BYTES */
		if (uart0_filestream != -1)
		{
            //printf("Something arrived\n");
			/* Read up to 255 characters from the port if they are there */
            /* Filestream, buffer to store in, number of bytes to read (max) */
			rx_length = read(uart0_filestream, (void*)rx_buffer, 1023);
			//printf("rx_length: %d\n", rx_length);
            if (rx_length < 0)
			{
				/* An error occured (will occur if there are no bytes) */
			}
			else if (rx_length == 0)
			{
				/* No data waiting */
			}
			else
			{
				/* Bytes received */
				rx_buffer[rx_length] = '\0';
                for (rx_buf_idx=0; rx_buf_idx<rx_length; rx_buf_idx++) {
                    /* Check for JSON start */
                    if (rx_buffer[rx_buf_idx] == '{') {
                        req_buf_idx = 0;
                        memset(serial_incoming, 0, sizeof(char)*256);
                        copy_buffer = 1;
                    }
                    if (copy_buffer) {
                        serial_incoming[req_buf_idx] = rx_buffer[rx_buf_idx];
                        req_buf_idx++;
                    }
                    /* Check for JSON end */
                    if (rx_buffer[rx_buf_idx] == '}') {
                        serial_incoming[req_buf_idx] = '\0';
                        copy_buffer = 0;
                        printf("request: %s\n", serial_incoming);

                        serial_incoming_len = strlen(serial_incoming);
                        json_to_request_params(serial_incoming, serial_incoming_len);

                        get_formated_timestamp(timestamp);
                        /* Send to Anemosi */
                        sprintf(request_link_anemosi.data_buffer,
                            "timestamp=%s&%s", timestamp, serial_incoming);
                        write_socket_blocking(&request_link_anemosi);
                        /* Send to local Node */
                        sprintf(request_link_node.data_buffer,
                            "timestamp=%s&%s", timestamp, serial_incoming);
                        write_socket_blocking(&request_link_node);
                    }
                }
			}
		}

        //write_socket_blocking(data_buffer, &request_link_anemosi);
        //write_socket_blocking(data_buffer, &request_link_node);
        //sleep(10);
    }

    return 0;
}


void get_formated_timestamp(char *timestamp) {
    // -- calendar time (Epoch time) - usually signed int
    time_t time_epoch;
    // -- pointer to time structure, for specifications view 'ctime' at 'man7'
    struct tm *time_human;
    // Timestamp buffer
    // -- save current time since Unix Epoch in seconds
    time_epoch = time(NULL);
    // -- save current broken down time to time structure
    time_human = localtime(&time_epoch);

    // -- convert to standardised timestamp
    sprintf(timestamp, "%04d-%02d-%02dT%02d:%02d",
        time_human->tm_year+1900, time_human->tm_mon+1, time_human->tm_mday,
        time_human->tm_hour, time_human->tm_min);
}
