#include "requests.h"

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */
#include <time.h>

char request_fmt[REQUEST_BUF_LEN] =
            "POST %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "Content-Type: application/x-www-form-urlencoded\r\n"
            "Content-Length: %d\r\n\r\n"
            "%s\r\n\r\n";

/* Validate host address, set up server structure
 *  returns:
 *   1 - setup successful
 *   0 - error setting up
 */
int setup_socket (request_link_t *request_link) {

    struct hostent *server;
    // -- lookup host
    server = gethostbyname(request_link->host_addr);
    if (server != NULL) {
        printf("SOCKET CREATED\n");
    } else {
        printf("SOCKET: UNKNOWN HOST: %s\n", request_link->host_addr);
        return -1;
    }

    memset(&request_link->serv_addr,0,sizeof(request_link->serv_addr));
    request_link->serv_addr.sin_family = AF_INET;
    request_link->serv_addr.sin_port = htons(request_link->portno);
    memcpy(&request_link->serv_addr.sin_addr.s_addr,
        server->h_addr,server->h_length);

    return 0;
}


/* Create, connect, write and close socket - blocking.
 */
int write_socket_blocking (request_link_t *request_link) {
    static int32_t sockfd;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    //printf("SOCKET CREATED\n");

    size_t data_len = strlen(request_link->data_buffer);
    sprintf(request_link->request, request_fmt, request_link->request_path,
        request_link->host_addr, data_len, request_link->data_buffer);
    //printf("*\tREQUEST LOADED: \n%s\n", request_link->request);

    if (connect(sockfd,(struct sockaddr *)&request_link->serv_addr,
    sizeof(request_link->serv_addr)) < 0) {
        printf("SOCKET CONNECT ERROR\n");
        return -1;
    }
    //printf("SOCKET CONNECTED\n");

    size_t sent = 0;
    size_t bytes_write, request_len;
    request_len = strlen(request_link->request);

    while (request_len > sent) {
        // (socket, buffer pointer + offset, number of characters to send)
        bytes_write = write(
            sockfd, request_link->request + sent, request_len - sent);
        sent+=bytes_write;
        printf("*\tREQUEST WRITTEN: \n%s\n", request_link->request);
        //printf("REQUEST LENGTH | SENT | BYTES_WRITE:\n%lu\t|\t%lu\t|\t%lu\n",
        //    request_len, sent, bytes_write);
    }

    close(sockfd);
    //printf("SOCKET CLOSED\n");

    return 0;
}



/* Change data format in data buffer
 *  Go from:"a=12&b=34&c=56"
 *  To: "{\"ab\":\"12\",\"cd\":\"34\",\"ef\":\"56\"}"
 */
void json_to_request_params(char *data, size_t data_len) {
    char data_tmp[data_len];
    int data_tmp_counter = 0;
    int i = 0;

    for (i=0; i<data_len; i++) {
        if (data[i] == '\"') {
        }
        switch (data[i]) {
        case '{':
        case '}':
        case '\"':
            // Skip
            break;
        case ':':
            data_tmp[data_tmp_counter] = '=';
            data_tmp_counter++;
            break;
        case ',':
            data_tmp[data_tmp_counter] = '&';
            data_tmp_counter++;
            break;
        default:
            data_tmp[data_tmp_counter] = data[i];
            data_tmp_counter++;
            break;
        }
    }

    memset(data, 0, data_len);
    //snprintf(data, data_tmp_counter+1, "%s", data_tmp);
    memcpy(data, data_tmp, data_tmp_counter);
}
