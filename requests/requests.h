#ifndef _REQUESTS_H_
#define _REQUESTS_H_

//#include "../include/supervisor.h"
//#include "../fifo/fifo.h"
//#include "../tasks/tasks.h"

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */
#include <time.h>
#include <stdint.h>


// -- debug
#ifndef DEBUG_REQUESTS
#define DEBUG_REQUESTS 0
#endif

#define HOST_ADDR_BUF_LEN       64
#define REQUEST_PATH_LEN        128
#define REQUEST_BUF_LEN         1024
#define DATA_BUF_LEN            256

typedef struct {
    struct sockaddr_in serv_addr;
    char host_addr[HOST_ADDR_BUF_LEN];
    uint16_t portno;
    char request_path[REQUEST_PATH_LEN];
    char request[REQUEST_BUF_LEN];
    char data_buffer[DATA_BUF_LEN];
} request_link_t;


/* Validate host address, set up server structure
 *  returns:
 *   1 - setup successful
 *   0 - error setting up
 */
int setup_socket (request_link_t *request_link);

/* Create, connect, write and close socket - blocking.
 */
int write_socket_blocking (request_link_t *request_link);

/* Change data format in data buffer.
 *  Go from:"a=12&b=34&c=56"
 *  To: "{\"ab\":\"12\",\"cd\":\"34\",\"ef\":\"56\"}"
 */
void json_to_request_params(char *data, size_t data_len);

#endif //_REQUESTS_H_
